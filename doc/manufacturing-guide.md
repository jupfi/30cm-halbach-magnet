# Manufacturing Guide

## General

Parts in the [BOM](/BOM.csv) with `Make=true` are meant to be manufactured or post-processed, following the instructions referenced in the `Reference` column.

Please find:

- native CAD files under [/src/CAD/](/src/CAD/)
- 3D models in exchange formats in the respective [release notes](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/releases)
- material specifications under [/src/material-specifications.csv](/src/material-specification.csv)

## Rings

- note on material: choose the color to your liking; however, we found that transparent PP behaves more "gluey" when milled, but is often cheaper
- smallest inner radius: 2.1mm (north marking of magnet pockets)
- smallest tolerance class: H7 (magnet pockets)

The material tends to bend during the milling process and chips tend to agglutinate when getting to warm, so it has to be fastened and the milling speed limited.

Here's an example of how this has been realized:

1. Mill blank ring with the following features:
	- outer contour
	- inner contour
	- thickness
	- fastening bores in the waste pieces of the shimming trays (or alternatively the outer Ø6mm bores)\
		![](/res/img/blank-ring.jpg){width=30%}\
		_blank ring_
2. Fasten blank ring onto a flat milled clamping plate.
	- It's also possible to do this for multiple rings in a batch.\
		![](/res/img/clamping-plate.jpg){width=30%}\
		_clamping plate_
		![](/res/img/blank-ring-on-clamping-plate.jpg){width=30%}\
3. mill all remaining features
	- to reduce heat when milling the magnet pockets, we bored a Ø11mm hole for each pocket first, followed by a Ø3mm cutter for milling the contour\
		![](/res/img/clamping-plate.jpg){width=30%}\
		_drilling Ø11mm holes (chips are vacuumed)_
	- remove the fasteners successively during the milling process; at the end you can use the outer Ø6mm bores for fastening
4. post-processing: deburr, remove chips from the pockets,…\
	![](/res/img/finished-front-ring.jpg){width=30%}\
	_finished front ring and front lid_

**Example setting:**

- single-edged cutters
- no lubrication
- vacuum for chip removal

| feature            | cutter Ø [mm] | rpm  | feed rate [mm/min] |
|--------------------|---------------|------|--------------------|
| shim tray holes    | 8             | 8000 | 1000               |
| pockets (contours) | 3             | 8000 | 550                |

## Ring Lids

- note on material: choose the color to your liking; however, translucent lids make trouble-shooting easier :)

double-sided tape fully suffices for fastening

## 3D Printed Parts

- `ShimTS`
- `ShimTE`
- `GradM`

## Lasercut Parts

- `ShimIn`
