# Assembly Guide

## General

> This guide gives instructions on how to assemble the magnet. For manufacturing instructions of the parts, please refer to the [manufacturing guide](/doc/manufacturing-guide.md)

The magnet consists of rings that hold a number of permanent magnets in a specific orientation.
The rings have to be milled, equipped with permanent magnets, closed with a lid and then mounted in a defined order using brass rods.

## Equipping the Rings

### Rings 1…8

Parts for this step:

```
R1
R2
R3
R4
R5
R6
R7
R8
RL
RL4
Mag12
M4hex12
```

For expectation management: calculate about 20…30min per ring.

1. Mark the polarity of all permanent magnets (`Mag12`), so you can easily identify north, e.g. like this:\
	![manually marked magnet | laser-engraved mark](/res/img/manual-laser-mark.jpg){width=30%}\
	_manually marked magnet | laser-engraved mark_
2. Before inserting the permanent magnets, make sure that the magnet pockets in the ring (`R1…8`) are clean and chip-free, see figure below\
	![detail of lint from milling](/res/img/lint-detail.jpg){width=30%}\
	_detail of lint from milling_
3. Insert the permanent magnets (`Mag12`) into the pockets of the ring `R1…8`.
	- **Make sure that all inserted magnets fulfill the quality requirements** (full field strength, no chipping etc.), otherwise you might end up with a severely inhomogeneous $B_0$-field later.\
	Please see [QM guide](/doc/qm-guide-md) for details.
	- The mark of the magnet pocket indicates north:\
		![detail of a magnet pocket](/res/img/magnet-pocket.jpg){width=30%}\
		_detail of a magnet pocket_
    - It's generally easier to first fill the inner ring of magnets before starting with the outer one. Convenient start and end positions are where one of the magnet poles points towards the center of the ring.
	    - We're talking 1936 permanent magnets in total for 2×8 rings. Please don't use your bare fingers for inserting them :)\
	    ![inserting magnets 1](/res/img/insert-magnets1.jpg){width=30%} ![inserting magnets 2](/res/img/insert-magnets2.jpg){width=30%}\
	    _inserting magnets_
	    - Watch your fingers!\
	    	![magnets trying to unmount themselves when inserting the next](/res/img/magnet-lift.jpg){width=30%}\
	    	_magnets trying to unmount themselves when inserting the next_
	    - Make your life easier with supporting tools, e.g. the [magnet stapler](https://gitlab.com/osii/tools/magnet-stapler).
4. Screw the lid (`RL`) on top of the ring (`R1…8`).
    - …using `M4hex12`:\
	    ![mounted ring lid](/res/img/mounted-lid.jpg){width=30%}\
	    _mounted ring lid_
	- **NOTE** to mount one `R8` with `RL4` and the other with `RL`, see the [magnet mounting step](#mounting-the-magnet) below.

Be aware of quality requirements while executing the steps above.
Any unrecognized flaw here can easily result in a lot of pain and work later on, since retrospective fixing most likely requires disassembling the magnet.

### Front and Back

Parts for this step:

```
FF
FL
BF
BL
SR
Mag50
M4hex50
```

For expectation management: calculate about 2h per ring.

1. As for [R1…8 above](#rings-18)):
    - mark the polarity of all magnets (`Mag50`) and
    - make sure that the magnet pockets in the rings and lids (`FF`, `FL`, `BF`, `BL`) are clean and chip-free before inserting the magnets
2. Insert the magnets (`Mag50`) into a ring with foot (`FF`/`BF`)
    - Handle the magnets individually and with care; they are equally as brittle as strong\
        ![separating 50mm magnets](/res/img/50mm-mag-seperation.jpg){width=30%}\
	    _separating 50mm magnets_
	- Start with the innermost circle and then go outwards.\
        ![first circle of mounted magnets](/res/img/50mm-mag-first-circle.jpg){width=30%}\
	    _first circle of mounted magnets_
	- Tools like the [magnet wrench](https://gitlab.com/osii/tools/magnet-wrench) or distance pieces (as for window installation) make your life easier\
        ![using tools for mounting the 50mm magnets](/res/img/50mm-mag-tools.jpg){width=30%}\
	    _using tools for mounting the 50mm magnets_
3. Put the spacer ring (`SR`) and install the lid (`FL`/`BL`) using a rubber hammer and screw `M4hex50` through lid and spacer ring into the foot ring

## Mounting the Magnet

Parts for this step:

- mounted rings with lids from [this step above](#rings-18)
- mounted front and back rings with lids from [this step above](#rings-18)

as well as:

```
M4sleeve
Spacer6x6
Spacer6x2
Spacer4x6
Spacer4x2
M6lnut
M4lnut
Rod6
Rod4
```

1. Screw `M6lnut` onto the end with the short thread of all `Rod6`
2. Screw `M4sleeve` onto the end with the short thread of all `Rod4`
3. Put all rods (`Rod6`, `Rod4`) through the corresponding holes of the mounted front part (`FF` | `SR` | `FL`) so that the "empty" ends of the rods face "inwards":
    ![Front part with rods](/res/img/front-rods.jpg){width=30%}\
	_Front part with rods_
4. With the front part on a flat surface (so that the rods point upwards) put 2× `spacer6x2` on all `Rod6` and install the `R8` _with `RL4`_ **facing down** (the mark of `R8` oriented "upwards", when the magnet would be standing on its feet)
    - **NOTE** the different spacer sizes in these steps as magnets need to get into the end positions as defined in the [magnet analytic text file](/src/magnet orientations/analytic-18 rings.txt)
5. Put `spacer6x6` on all `Rod6`, `spacer4x6` on all `Rod4` and proceed with `R7`, again facing down (mark of `R7` upwards)
6. Repeat the prior step for `R6`…`R1` and then `R1`…`R8`. You can use [these clamps](https://gitlab.com/osii/tools/magnet-clamp/) to make your life easier.
7. Put `spacer6x2` on all `Rod6`, `spacer4x2` on all `Rod4` and install the mounted back part (`BL` | `SR` | `BF`). Lock the entire stack with `M6lnut` and `M4lnut` on the rods (`Rod6`, `Rod4`).
8. Put the magnet on its feet.

For disambiguation:

![Schematic ring mounting](/res/drw/ring-mounting.svg){width=50%}\
_Schematic ring mounting_

## Shim Trays

1. Print the shim trays
	- source file: `/src/CAD/Shim Tray.par`
	- please find the STL export in the respective [release notes](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/releases)
2. Number shim trays
3. Insert shim trays

