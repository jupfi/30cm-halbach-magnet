# Guide for Quality Management

PLACEHOLDER

- [TABLE] common magnet flaws (chipping, demagnetization, cracks etc.)
- Quality check of rings:

	- Thickness of 20mm and 15mm PP sheets! (PC sheets can have higher tolerances)
	- Overall tolerance is ISO 2768-m (all rings)
	- H7 fit (also on top!)
	- Warping of 20mm sheets while milling!

## Notes

- Magnets should be a tight press fit but never have to be hammered in. When turning the ring upside down the magnets shouldn't fall out.

- Watch out for the corners in the pockets – since the cutter has to reach quite far into it, it will often bend away a bit and you end up with conical pockets. just give the cutter an extra round and you should be good