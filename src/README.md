# The CAD pipeline

STEP EXPORT HERE: https://box.ptb.de/getlink/fiAR5fibX414pVzfczCSYcg4/STEP-export-240304 (v240304)

## General

- $B_0$ (north) is in negative x-direction of the coordinate system of the FreeCAD macro (`rotatingMagnets.FCMacro`), and so also of the generated orientation files in the [magnet orientations](/src/magnet%20orientations/) folder; $B_0$ has to point "upwards" in all rings
- orientations and CAD files relate as shown in the following table
	- **NOTE** that the `analytic-18 rings.txt` also indicates the distance between magnet center points in axial (z) direction (column 3) which may _not_ be the same for all; hence the distance between `R8` and `FL` may be another one than between `R8` and `R7`.

| Orientation file | CAD file       | BOM ID |
|------------------|----------------|--------|
| ring0            | ring1.par      | R1     |
| ring1            | ring2.par      | R2     |
| ring2            | ring3.par      | R3     |
| ring3            | ring4.par      | R4     |
| ring4            | ring5.par      | R5     |
| ring5            | ring6.par      | R6     |
| ring6            | ring7.par      | R7     |
| ring7            | ring8.par      | R8     |
| ring8 (mirrored) | front-foot.par | FF     |
| ring8            | front-lid.par  | FL     |
| ring8            | back-foot.par  | BF     |
| ring8 (mirrored) | back-lid.par   | BL     |

## Making the magnet pockets

Ring files are created from a template file (`ring-template.par`) and generated magnet pocket files:

1. copy the FreeCAD macro (`rotatingMagnets.FCMacro`) into your local FreeCAD macro directory
	> e.g. `/home/USER/.var/app/org.freecadweb.FreeCAD/data/FreeCAD/Macro/` if you're on Linux and have FreeCAD installed over flathub
2. define the ring for which the macro should be executed by changing the file path in line 19 of the macro → point to the corresponding raw ring data file; please see the table above for how the magnet orientation files correspond to the actual ring files
	> e.g. `/home/USER/git/30cm-halbach-magnet/src/magnet orientations/raw/ring 0.txt`
3. execute macro in FreeCAD (top menu bar: Macro → Macros… → select rotatingMagnets.FCMacro → Execute
4. mark all shapes, export as `DXF`
6. import `DXF` file in other CAD software (e.g. SolidEdge or SolidEdge) to extrude the sketch
	> NOTE: if you're using SolidEdge for the extrusion, use the selection mode "single" (not "chain") and then mark the whole sketch
7. copy the template ring file (`ring-template.par`) and adjust the mark on the top indicating the ring number
8. create an assembly from the new ring file, load the solid magnet pockets and subtract them from the ring
	> NOTE: double check the orientation! sometimes SolidEdge mirrors the pockets without any notice
9. save the assembly file; this will also save the changes for the ring file
10. repeat for all rings :)

## Rods, screws and lids

All bores for screws and lids should be identical for all rings, since this gives us: 1) one lid design for all rings and 2) a single template ring file for all rings.

Bores on a circle outside the magnet pockets are trivial; the tricky part is to fit the inner bores _between_ the pockets.

For this, you can execute the FreeCAD macro for all rings in one single project and allocate convenient spots for the holes, see screenshot below.

> Alternatively, to save computation power, you can also bore envelope circles into a dummy plate as done in `screw-positioning.FCStd`

![Finding a good spot for a hole](/res/img/magnet-overlap-hole.png){width=60%}\
_Finding a good spot for a hole_

**EXAMPLE:**

The current design has:

- 1st row: 57 magnets on $r_1=171mm$
- 2nd row: 64 magnets on $r_2=192mm$
- 3rd row: 71 magnets on $r_3=213mm$ (only front and back rings)

This means that there should be 57 convenient spots for bores.
The FreeCAD macro starts on the positive x-axis; on the opposite end (negative x-axis) we find a convenient spot for a first bore on a radius $r_{bores}=179.5mm$ (see screenshot above).

For our current design we wanted to use 8 inner rods and 12 inner screws.

If we define the bore the closest to positive $B_0$ as position 1, a setup could look like this (quoted from `bore-positioning.ods`):

| Pos. | Type  | Deg from B_0 (CW) |
|------|-------|-------------------|
| 1    | rod   | 0,000             |
| 6    | screw | 31,579            |
| 8    | rod   | 44,211            |
| 11   | screw | 63,158            |
| 14   | screw | 82,105            |
| 15   | rod   | 88,421            |
| 20   | screw | 120,000           |
| 22   | rod   | 132,632           |
| 25   | screw | 151,579           |
| 29   | screw | 176,842           |
| 30   | rod   | 183,158           |
| 34   | screw | 208,421           |
| 36   | rod   | 221,053           |
| 39   | screw | 240,000           |
| 43   | screw | 265,263           |
| 44   | rod   | 271,579           |
| 49   | screw | 303,158           |
| 51   | rod   | 315,789           |
| 53   | screw | 328,421           |
| 57   | screw | 353,684           |

**Making changes:**

- ring template changes (e.g. bore positions)
	> 1. change ring template
	> 2. copy the template file and adjust the mark on the top indicating the ring number; use the filename of the desired ring file when saving (e.g. `ring1.par`)
	> 3. replace the ring file with the new one
	> 4. open the corresponding assembly file for the magnet pocket subtraction and repeat the subtraction; SolidEdge links assembly parts by filename, so you should see the new ring in the assembly
- magnet rotation changes
	> 1. replace the file with the solid magnet pockets with the newly generated one
	> 2. open the corresponding assembly file for the magnet pocket subtraction and repeat the subtraction; SolidEdge links assembly parts by filename, so you should see the new ring in the assembly