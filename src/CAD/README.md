# CAD Files

CAD models have been created with Solid Edge Version `2210.0007 (Build 223.00.07.005 x64)`.

For STEP exports please refer to the respective [release](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/releases).
